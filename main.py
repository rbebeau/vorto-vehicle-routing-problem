"""
Developer: Robert Bebeau
email: rbebeau@gmail.com

This code finds the optimal route for a modified form the of the Vehicle Routing Problem
with direct pickups to deliveries.
"""

# initial example provided by:  https://developers.google.com/optimization/routing/pickup_delivery
import sys
import math
import numpy as np
from scipy.spatial import distance_matrix
from ortools.constraint_solver import routing_enums_pb2
from ortools.constraint_solver import pywrapcp

# set run parameters
inflation_factor = 100
time_limit = 29  # limit on the search in seconds
debug = False

def load_data(filename):
    """ Load data from file and return coordinates, and pickups_delivery matrix """
    with open(filename, 'r') as file:
        lines = file.readlines()
        n_lines = len(lines)  # just count the number of lines.

        pickups_deliveries = []

        coords = np.zeros((2*n_lines-1, 2), dtype=float)

        for i, line in enumerate(lines):
            if i == 0: continue # skip header line

            pickups_deliveries.append([i, i+n_lines-1])

            x1 = float(line.split(' ')[1].replace('(', '').replace(')', '').split(',')[0])
            y1 = float(line.split(' ')[1].replace('(', '').replace(')', '').split(',')[1])

            x2 = float(line.split(' ')[2].replace('(', '').replace(')', '').split(',')[0])
            y2 = float(line.split(' ')[2].replace('(', '').replace(')', '').split(',')[1])

            coords[i, 0] = x1
            coords[i, 1] = y1
            coords[i+n_lines-1, 0] = x2
            coords[i+n_lines-1, 1] = y2

    return coords, pickups_deliveries


def print_solution(data, manager, routing, solution, inflation_factor):
    """Prints solution on console."""
    print(f"Objective: {solution.ObjectiveValue()}")
    total_distance = 0
    for vehicle_id in range(data["num_vehicles"]):
        index = routing.Start(vehicle_id)
        plan_output = f"Route for vehicle {vehicle_id}:\n"
        route_distance = 0
        while not routing.IsEnd(index):
            plan_output += f" {manager.IndexToNode(index)} -> "
            previous_index = index
            index = solution.Value(routing.NextVar(index))
            route_distance += routing.GetArcCostForVehicle(
                previous_index, index, vehicle_id
            )
        plan_output += f"{manager.IndexToNode(index)}\n"
        plan_output += f"Distance of the route: {(route_distance/inflation_factor)}m\n"
        print(plan_output)
        total_distance += route_distance
    print(f"Total Distance of all routes: {(total_distance/inflation_factor)}m")


def print_pickup_nodes(data, manager, routing, solution, number_nodes):
    """Prints solution in format for evaluateShared.py"""
    for vehicle_id in range(data["num_vehicles"]):
        node_stops = []

        index = routing.Start(vehicle_id)
        route_distance = 0
        while not routing.IsEnd(index):
            previous_index = index
            index = solution.Value(routing.NextVar(index))
            route_distance += routing.GetArcCostForVehicle(
                previous_index, index, vehicle_id
            )
            node_stops.append(manager.IndexToNode(index))

        if route_distance > 0:
            node_stops = [e for e in node_stops if (e < number_nodes/2) and e != 0]
            print(node_stops)


def main():
    file_path = sys.argv[1] # read in filepath from command line

    coords, pickups_deliveries = load_data(file_path) # load data from file
    distance = distance_matrix(coords, coords)
    distance = np.round(distance * inflation_factor)

    num_drivers = math.ceil((distance.shape[0] * np.max(distance[0, :]) + 500 * inflation_factor) / (12 * 60 * inflation_factor))

    data = {}
    data["distance_matrix"] = distance.astype(int)
    data["pickups_deliveries"] = pickups_deliveries
    data["num_vehicles"] = num_drivers
    data["depot"] = 0

    if debug: print(data)

    # Create the routing index manager.
    manager = pywrapcp.RoutingIndexManager(
        len(data["distance_matrix"]), data["num_vehicles"], data["depot"]
    )

    # Create Routing Model.
    routing = pywrapcp.RoutingModel(manager)

    # Define cost of each arc.
    def distance_callback(from_index, to_index):
        # Convert from routing variable Index to distance matrix NodeIndex.
        from_node = manager.IndexToNode(from_index)
        to_node = manager.IndexToNode(to_index)
        return data["distance_matrix"][from_node][to_node]

    transit_callback_index = routing.RegisterTransitCallback(distance_callback)
    routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)

    # Add Distance constraint.
    dimension_name = "Distance"
    routing.AddDimension(
        transit_callback_index,
        0,  # no slack
        12*60*inflation_factor,  # vehicle maximum travel distance
        True,  # start cumul to zero
        dimension_name,
    )
    distance_dimension = routing.GetDimensionOrDie(dimension_name)
    routing.SetFixedCostOfAllVehicles(500*inflation_factor)  # gets added to the distance - which is lame.


    # Define Transportation Requests.
    for request in data["pickups_deliveries"]:
        pickup_index = manager.NodeToIndex(request[0])
        delivery_index = manager.NodeToIndex(request[1])
        routing.AddPickupAndDelivery(pickup_index, delivery_index)
        routing.solver().Add(
            routing.VehicleVar(pickup_index) == routing.VehicleVar(delivery_index)
        )
        routing.solver().Add(routing.NextVar(pickup_index) == delivery_index)
        routing.solver().Add(distance_dimension.CumulVar(pickup_index) <= distance_dimension.CumulVar(delivery_index))  # modify for direct pickup to delivery

    # Setting first solution heuristic.
    search_parameters = pywrapcp.DefaultRoutingSearchParameters()
    search_parameters.first_solution_strategy = (
        routing_enums_pb2.FirstSolutionStrategy.PARALLEL_CHEAPEST_INSERTION
    )
    search_parameters.time_limit.seconds = time_limit

    # Solve the problem.
    solution = routing.SolveWithParameters(search_parameters)

    # Print solution on console.
    if solution:
        if debug: print_solution(data, manager, routing, solution, inflation_factor)
        print_pickup_nodes(data, manager, routing, solution, distance.shape[0])


if __name__ == "__main__":
    main()