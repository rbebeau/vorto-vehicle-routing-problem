# Vorto Vehicle Routing Problem (VRP)

## Introduction
This program solves a modified vehicle routing problem with direct pickup to delivery.
It is intended to minimize the total distance a fleet of vehicles spends making a series of transports defined by a set
of pickup coordinates, delivery coordinates. These transports are defined in the Training Problems folder in each of the
problem#.txt files.

The program uses the OR-Tools package for optimization.

## Setup
This project is Python based.  Please install the following tools and dependencies (recommended versions are shown in parentheses):
<ul>
<li> Python3 (3.10)
<li> NumPy (1.26.4)
<li> SciPy (1.12.0)
<li> OR-Tools (9.9.3963)
</ul>

A requirements.txt file is provided for convenience, but should not be relied upon.

## Run instructions

There are two ways to run this project in a single file more and in a multi-file evaluation mode.  

### Single Problem Run
The single file mode uses the main.py function, which contains the optimization routine, directly.

To run a single problem file run:

```python main.py {path_to_problem}```

### Evaluation Run
To run a batch of problem files and obtain the mean objective with error checks, run:

```python3 evaluateShared.py --cmd "python3 main.py" --problemDir "{bulk_problem_file_directory}"```

Note: debug option must be False for evaluation run.

### Run Parameters
The code provides some options for running.

<ul>
<li> inflation_factor: Number of decimals to include in calculation. Default: 100
<li> time_limit: Time in seconds allowed for the optimization search. Default: 29
<li> debug: Flag for printing additional output. Default: False
</ul>

## Author and References
Devloper: Robert Bebeau - rbebeau@gmail.com

Reference:
Google OR-Tools (https://developers.google.com/optimization/routing/pickup_delivery)
Vorto